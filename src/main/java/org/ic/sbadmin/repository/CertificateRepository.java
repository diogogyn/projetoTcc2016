package org.ic.sbadmin.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.ic.sbadmin.config.ConnectionDB;
import org.ic.sbadmin.model.Certificate;
import org.ic.sbadmin.model.CertificateDescription;

/**
 * 
 *          CLASSE QUE VAI REALIZAR A PERSISTeNCIA DO NOSSO OBJETO ProfissionalModel
 *          NO BANCO DE DADOS.
 * 
 * 
 */
@Repository
public class CertificateRepository {

	/**
	 * @PersistenceContext é o local onde ficam armazenados as entidades que
	 *                     estao sendo manipuladas pelo EntityManager
	 * 
	 * 
	 * @PersistenceContext(type = PersistenceContextType.EXTENDED) assim o
	 *                          servidor vai gerenciar para nos as entidades.
	 * 
	 **/
	//@PersistenceContext(type = PersistenceContextType.EXTENDED)
	//private EntityManager manager;
	private Connection con;

	public CertificateRepository() throws SQLException, ClassNotFoundException {
		this.con =  ConnectionDB.getConnection();
	}
	/**
	 * 
	 * @param certificate
	 * 
	 *            Salva um novo registro
	 * 
	 *            O JPA exige um contexto de transacao para realizar as
	 *            alteracoes, por isso vamos usar a
	 *            anotacao @javax.transaction.Transactional
	 * 
	 */
	//@javax.transaction.Transactional
	public void Salve(CertificateDescription certificate) {
		try{
			// cria um preparedStatement
			PreparedStatement stmt = this.con.prepareStatement("insert into certificado (nome, certificado, issuerdn, subjectdn, notbefore, notafter) values (?, ?, ?, ?, ?, ?)");
			// preenche os valores
			stmt.setString(1, certificate.getName());
			stmt.setString(2, certificate.getCertificate());
			stmt.setString(3, certificate.getIssuerDN());
			stmt.setString(4, certificate.getSubjectDN());
			stmt.setString(5, certificate.getNotBefore());
			stmt.setString(6, certificate.getNotAfter());

			// executa
			stmt.execute();
			stmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		//manager.persist(certificate);
	}

	/**
	 * 
	 * @param idCertificate
	 * @return Certificado
	 * 
	 *         Consulta um certificado pelo ID
	 */
	public CertificateDescription SearchByCode(int idCertificate) {
		CertificateDescription c = null;
		try {//futuramente implementar via hibernate. Apenas para proposito de teste
			PreparedStatement stmt = this.con.prepareStatement("SELECT id, nome, issuerdn, subjectdn, notbefore, notafter FROM certificado WHERE id=? ORDER BY id");
			stmt.setInt(1, idCertificate);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				c = new CertificateDescription(rs.getInt("id"),rs.getString("nome"),"",rs.getString("issuerdn"),rs.getString("subjectdn"),rs.getString("notbefore"),rs.getString("notafter"));
			}

			rs.close();
			stmt.close();
			return c;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return c;
		}
		//return manager.find(Certificate.class, idCertificate);
	}

	/**
	 * 
	 * @param idCertificate
	 *
	 *            Exclui uma via por ID
	 */
	//@javax.transaction.Transactional
	public boolean Delete(int idCertificate) {
		Certificate certificate = this.SearchByCode(idCertificate);
		try{
			// cria um preparedStatement
			PreparedStatement stmt = this.con.prepareStatement("DELETE FROM certificado WHERE id=?");
			// preenche os valores
			stmt.setInt(1, idCertificate);
			// executa
			stmt.execute();
			stmt.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		//manager.remove(certificate);
	}

	/**
	 * 
	 * @return List<Certificate>
	 * 
	 *         Consulta todos os Certificate no banco de
	 *         dados
	 */
	public List<Certificate> AllCertificates() {
		List<Certificate> list = new ArrayList<Certificate>();
		try {//futuramente implementar via hibernate. Apenas para proposito de teste
			PreparedStatement stmt = this.con.prepareStatement("SELECT id, nome FROM certificado ORDER BY id");
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				list.add(new Certificate(rs.getInt("id"),rs.getString("nome"),""));
			}

			rs.close();
			stmt.close();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		return list;
	}

}
