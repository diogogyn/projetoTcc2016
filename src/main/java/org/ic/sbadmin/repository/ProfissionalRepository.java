package org.ic.sbadmin.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.ic.sbadmin.model.Profissional;
import org.springframework.stereotype.Repository;

/**
 * @author: cicero.ednilson
 * 
 *          CLASSE QUE VAI REALIZAR A PERSISTeNCIA DO NOSSO OBJETO ProfissionalModel
 *          NO BANCO DE DADOS.
 * 
 * 
 */
@Repository
public class ProfissionalRepository {

	/**
	 * @PersistenceContext é o local onde ficam armazenados as entidades que
	 *                     estao sendo manipuladas pelo EntityManager
	 * 
	 * 
	 * @PersistenceContext(type = PersistenceContextType.EXTENDED) assim o
	 *                          servidor vai gerenciar para nos as entidades.
	 * 
	 **/
	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	private EntityManager manager;

	/**
	 * 
	 * @param viaModel
	 * 
	 *            Salva um novo registro
	 * 
	 *            O JPA exige um contexto de transacao para realizar as
	 *            alteracoes, por isso vamos usar a
	 *            anotacao @javax.transaction.Transactional
	 * 
	 */
	@javax.transaction.Transactional
	public void Salvar(Profissional viaModel) {

		manager.persist(viaModel);
	}

	/**
	 * 
	 * @param viaModel
	 * 
	 *            Realiza a alteracao de um registro
	 */
	@javax.transaction.Transactional
	public void Alterar(Profissional viaModel) {

		manager.merge(viaModel);
	}

	/**
	 * 
	 * @param idProfissional
	 * @return Profissional
	 * 
	 *         Consulta uma via pelo ID
	 */
	public Profissional ConsultarPorCodigo(int idProfissional) {

		return manager.find(Profissional.class, idProfissional);
	}

	/**
	 * 
	 * @param idProfissional
	 * 
	 *            Exclui uma via por ID
	 */
	@javax.transaction.Transactional
	public void Excluir(int idProfissional) {

		Profissional viaModel = this.ConsultarPorCodigo(idProfissional);

		manager.remove(viaModel);

	}

	/**
	 * 
	 * @return List<Profissional>
	 * 
	 *         Consulta todos os profissionais no banco de
	 *         dados
	 */
	public List<Profissional> TodosProfissionais() {

		return manager.createQuery("SELECT c FROM Profissional c ORDER BY c.nome ", Profissional.class).getResultList();
	}

}
