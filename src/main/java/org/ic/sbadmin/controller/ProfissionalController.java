package org.ic.sbadmin.controller;

import java.util.List;

import org.ic.sbadmin.model.Profissional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import org.ic.sbadmin.model.ResultadoModel;
import org.ic.sbadmin.repository.ProfissionalRepository;

public class ProfissionalController {
	/**
	 * @Autowired => injetando o objeto resultadoModel no nosso controller
	 */
	@Autowired
	ResultadoModel resultadoModel;

	/**
	 * Injetando o objeto profissionalRepository
	 */
	@Autowired
	ProfissionalRepository profissionalRepository;

	/**
	 * Chama a view (editarRegistro.jsp) para editar um registro cadastrado
	 * 
	 */
	@RequestMapping(value = "/gerenciaProfisisonal/{idProfissional}", method = RequestMethod.GET)
	public ModelAndView GerenciarPosto(@PathVariable int idProfissional) {

		Profissional profissionalModel = profissionalRepository.ConsultarPorCodigo(idProfissional);

		return new ModelAndView("gerenciaProfisisonal", "profissionalModel", profissionalModel);
	}

	/**
	 * 
	 * Salva um novo registro via ajax, esse metodo vai ser chamado pelo
	 * cadastraoProfissionalCtrl.js atraves do AngularJS
	 * 
	 */
	@RequestMapping(value = "/salvarProfissional", method = RequestMethod.POST)
	public @ResponseBody ResultadoModel Salvar(@RequestBody Profissional profissionalModel) {

		try {

			profissionalRepository.Salvar(profissionalModel);

			resultadoModel.setCodigo(1);
			resultadoModel.setMensagem("Profissional cadastrado com sucesso!");

		} catch (Exception e) {

			resultadoModel.setCodigo(2);
			resultadoModel.setMensagem("Erro ao salvar o profissional (" + e.getMessage() + ")");
		}

		return resultadoModel;
	}

	/**
	 * Altera um registro cadastrado (editarRegistroController.js)
	 * 
	 *
	 */
	@RequestMapping(value = "/alterar", method = RequestMethod.POST)
	public @ResponseBody ResultadoModel Alterar(@RequestBody Profissional profissionalModel) {

		try {

			profissionalRepository.Alterar(profissionalModel);

			resultadoModel.setCodigo(1);
			resultadoModel.setMensagem("Registro alterado com sucesso!");

		} catch (Exception e) {

			resultadoModel.setCodigo(2);
			resultadoModel.setMensagem("Erro ao salvar o registro (" + e.getMessage() + ")");
		}

		return resultadoModel;
	}

	/**
	 * Consulta todos os registros cadastrados(consultarRegistrosController.js)
	 * 
	 */
	@RequestMapping(value = "/consultarTodosProfissionais", method = RequestMethod.GET)
	public @ResponseBody List<Profissional> ConsultarTodos() {

		return profissionalRepository.TodosProfissionais();// trocar

	}

	/**
	 * Excluir um usuario pelo codigo (consultarRegistrosController.js)
	 * 
	 */
	@RequestMapping(value = "/excluirRegistro/{idProfissional}", method = RequestMethod.DELETE)
	public @ResponseBody void ExcluirRegistro(@PathVariable int idProfissional) {

		profissionalRepository.Excluir(idProfissional);
	}

}
