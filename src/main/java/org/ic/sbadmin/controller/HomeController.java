package org.ic.sbadmin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	@GetMapping("/")
	public String home() {
		return "index2";
	}

	@RequestMapping("/index")
	public String index() {
		return "home/home";
	}
	
	@RequestMapping("/echo")
	public String echo() {
		return "home/home";
	}
	/*
	 *  @RequestMapping => value  => Defini o caminho para a chamada da view. 
	 * @RequestMapping => method => Defini o o m�todo http que o m�todo vai responder.
	 */
	@RequestMapping("/listaProfissionais")
	public String listaProfissionais() {
		return "profissional/listaProfissionais";
	}

	@RequestMapping("/cadastroProfissional")
	public String cadastroProfissional() {
		return "profissional/cadastroProfissional";
	}

}
