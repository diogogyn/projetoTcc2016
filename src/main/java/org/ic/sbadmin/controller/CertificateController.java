package org.ic.sbadmin.controller;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import org.ic.sbadmin.model.Certificate;
import org.ic.sbadmin.model.CertificateDescription;
import org.ic.sbadmin.model.CertificateFile;
import org.ic.sbadmin.repository.CertificateRepository;


import java.io.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

@RestController
public class CertificateController {

        @Autowired
    CertificateRepository certificateRepository = null;

    @RequestMapping(value = "/insertCertificate", method = RequestMethod.POST)
    public @ResponseBody boolean insertCertificate(@RequestBody CertificateFile c) throws CertificateException, FileNotFoundException {
        System.out.println("O metodo insertCertificate...");

        //fazendo esta subistituilção apenas até corrigir o problema do path
        /* ver se assim ira corrigir o problema
         File f = chooser.getSelectedFile();
        fileName = f.getAbsolutePath().toString();

         */
        c.setCertificadoFile(c.getcertificateFile().replace("fakepath","Users\\Diogo\\Desktop"));
        CertificateDescription certificat = new CertificateDescription();

        try{
            File publicKeyFile = new File(c.getcertificateFile());
            CertificateFactory fact = CertificateFactory.getInstance("X.509");
            FileInputStream is = new FileInputStream (publicKeyFile);
            X509Certificate cer = (X509Certificate) fact.generateCertificate(is);
            //PublicKey key = cer.getPublicKey();
            BufferedReader br = new BufferedReader(new FileReader(c.getcertificateFile()));
            String sCurrentLine;
            String certificate = "";
            while ((sCurrentLine = br.readLine()) != null) {
                //System.out.println(sCurrentLine);
                certificate+=sCurrentLine+"\n";
            }
            certificat.setName(c.getName());
            certificat.setCertificate(certificate);
            certificat.setSubjectDN(cer.getSubjectDN().toString());
            certificat.setIssuerDN(cer.getIssuerDN().toString());
            certificat.setNotBefore(cer.getNotBefore().toString());
            certificat.setNotAfter(cer.getNotAfter().toString());
            //certificat.setCertificateDescription();
            certificateRepository.Salve(certificat);

            return true;
        }catch (Exception e){
            System.err.println(e);
            return false;
        }
    }

    @RequestMapping(value = "/listCertificates", method = RequestMethod.GET)
    public @ResponseBody String listCertificates() {
        Gson gson = new Gson();
        return gson.toJson(certificateRepository.AllCertificates());
    }

    @RequestMapping(value = "/deleteCertificate/{id}", method = RequestMethod.GET)
    public @ResponseBody boolean deleteCertificate(@PathVariable("id") int id) {
        return certificateRepository.Delete(id);
    }

    @RequestMapping(value = "/viewCertificate/{idCertificate}", method = RequestMethod.GET)
    public @ResponseBody CertificateDescription viewCertificate(@PathVariable("idCertificate") int idCertificate) {
        return certificateRepository.SearchByCode(idCertificate);
    }
}
