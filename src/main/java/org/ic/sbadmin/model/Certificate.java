package org.ic.sbadmin.model;

import java.io.Serializable;


/**
 * The persistent class for the certificado database table.
 * 
 */
public class Certificate implements Serializable {
	private static final long serialVersionUID = 1L;

	//@Id
	//@Column(name="id")
	protected int id;

	//@Column(name="nome")
	protected String name;

	//@Column(name="certificado")
	protected String certificate;

	public Certificate() {
	}

	public Certificate(int id, String name, String certificate) {
		this.id = id;
		this.name = name;
		this.certificate = certificate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCertificate() {
		return certificate;
	}

	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}
}