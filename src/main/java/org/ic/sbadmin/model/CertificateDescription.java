package org.ic.sbadmin.model;

public class CertificateDescription extends Certificate {
    private String IssuerDN;
    private String SubjectDN;
    private String NotBefore;
    private String NotAfter;
    private String certificateDescription;

    public CertificateDescription() {
        IssuerDN = null;
        SubjectDN = null;
        NotBefore = null;
        NotAfter = null;
        certificateDescription = null;
    }

    public CertificateDescription(int id, String name, String certificate, String issuerDN, String subjectDN, String notBefore, String notAfter) {
        super(id, name, certificate);
        IssuerDN = issuerDN;
        SubjectDN = subjectDN;
        NotBefore = notBefore;
        NotAfter = notAfter;
    }

    public String getIssuerDN() {
        return IssuerDN;
    }

    public void setIssuerDN(String issuerDN) {
        IssuerDN = issuerDN;
    }

    public String getSubjectDN() {
        return SubjectDN;
    }

    public void setSubjectDN(String subjectDN) {
        SubjectDN = subjectDN;
    }

    public String getNotBefore() {
        return NotBefore;
    }

    public void setNotBefore(String notBefore) {
        NotBefore = notBefore;
    }

    public String getNotAfter() {
        return NotAfter;
    }

    public void setNotAfter(String notAfter) {
        NotAfter = notAfter;
    }

    public void setCertificateDescription(){
        certificateDescription = "- SubjectDN: "+SubjectDN+"<br>"
        +"- IssuerDN: "+IssuerDN+"<br>"
        +"- NotBefore: "+NotBefore+"<br>"
        +"- NotAfter: "+NotAfter;
        setCertificate(certificateDescription);
    }
}
