package org.ic.sbadmin.model;

import java.io.Serializable;


/**
 * The persistent class for the certificado database table.
 * 
 */
public class CertificateFile implements Serializable{
	private static final long serialVersionUID = 1L;
	private String name;

	private String certificateFile;
	public CertificateFile() {
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getcertificateFile() {
		return certificateFile;
	}

	public void setCertificadoFile(String certificateFile) {
		this.certificateFile = certificateFile;
	}
}