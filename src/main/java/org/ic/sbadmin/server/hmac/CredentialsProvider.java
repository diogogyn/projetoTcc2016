package org.ic.sbadmin.server.hmac;

public interface CredentialsProvider {

    byte[] getApiSecret(String apiKey);
}
