package org.ic.sbadmin.server.model;

public class FooResponseWrapper extends AbstractResponseWrapper<Foo> {

    public FooResponseWrapper() {
    }

    public FooResponseWrapper(Foo data) {
        super(data);
    }
}
