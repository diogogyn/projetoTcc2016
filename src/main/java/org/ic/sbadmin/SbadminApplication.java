package org.ic.sbadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class SbadminApplication {

	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SbadminApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(SbadminApplication.class, args);
	}
}
