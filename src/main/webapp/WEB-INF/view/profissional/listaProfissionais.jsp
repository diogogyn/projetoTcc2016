<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Cadastro</h1>
	</div>
</div>
<!-- /.row -->
<div class="row" ng-controller="listaProfissionaisCtrl">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Pesquisar</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">
					<form method="POST" action="#">
						<div class="col-lg-3">
							<div class="form-group">
								<label>Nome do Profissional:</label> <input class="form-control"
									placeholder="informe o nome do paciente">
							</div>
						</div>
						<!-- /.col-lg-3 -->
						<div class="col-lg-2">
							<div class="form-group">
								<br>
								<button type="submit" class="form-control btn btn-primary">
									<i class="fa fa-search"></i> Pesquisar
								</button>
							</div>
						</div>
						<!-- /.col-lg-2 -->
						<div class="col-lg-3">
							<div class="form-group">
								<br> <a href="#/cadastroProfissional"
									class="form-control btn btn-success"><i class="fa fa-plus"></i>
									Novo Profissional</a>
							</div>
						</div>
						<!-- /.col-lg-3 -->
					</form>
				</div>
				<!-- row -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel panel-default -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="well">
			<div class="row">
				<div class="col-lg-4" ng-repeat="profissional in listaProfissionais">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<div class="row">
								<div class="col-lg-8">{{profissional.nome}}</div>
								<div class="col-lg-4">
									<a href="gerencia_paciente.html" class="btn btn-warning btn-xs">Gerenciar</a>
								</div>
							</div>
							<!-- /.row -->
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-3">
									<img src="http://fakeimg.pl/60/?text=IMG">
								</div>
								<!-- /.col-lg-4 -->
								<div class="col-lg-8">
									<p>
										<b>Telefone:</b> {{profissional.telefone}}<br> <b>Data de Nascimento:</b> {{profissional.dtNascimento}}
								</div>
								<!-- /.col-lg-8 -->
							</div>
							<!-- /.row -->
						</div>
						<div class="panel-footer">
							<b>Função:</b> {{profissional.funcao}}
						</div>
					</div>
				</div>
				<!-- /.col-lg-4 -->

			</div>
			<!-- /.row -->
		</div>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
