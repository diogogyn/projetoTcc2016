<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Cadastro</h1>
	</div>
</div>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Cadastro de Profissional</div>
			<div class="panel-body">
				<form role="form">
					<div class="panel-group" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									Inclusão/Alteração
								</h4>
							</div>
							<div id="collapseInclusaoAlteracao"
								class="panel-collapse collapse in">
								<div class="panel-body">
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label>Nome Completo:</label>
												<input class="form-control" name="nome" ng-model="nome" placeholder="Nome do profissional/usuário">
											</div>
											<div class="form-group">
												<label>Funçao:</label>
												<select class="form-control" ng-options="funcao in funcoes" name="funcao" ng-model="funcao">
													<option value="">Selecione ...</option>
												</select>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label>UF:</label>
												<select class="form-control" name="uf" ng-model="uf">
													<option value="">GO</option>
													<option value="">TO</option>
													<option value="">SP</option>
												</select>
											</div>
											<div class="form-group">
												<label>Login:</label>
												<input class="form-control" name="login" ng-model="login" placeholder="username">
											</div>
											<div class="form-group">
												<label>Senha:</label>
												<input class="form-control" name="senha" ng-model="senha" placeholder="*******">
											</div>
											<div class="form-group">
												<label>Confirme a Senha:</label>
												<input class="form-control" name="senha2" ng-model="senha2" placeholder="*******">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
					<button ng-click="cadastrarProfissional()" class="btn btn-primary">Salvar</button>
				</form>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
</div>
