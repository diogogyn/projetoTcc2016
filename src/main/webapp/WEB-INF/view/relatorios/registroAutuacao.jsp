<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Registro de autuação</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row" ng-controller="registroAutuacaoCtrl">
	<div class="col-lg-12">
		<div class="panel-group" id="accordion">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">Notificação de infração</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-2">
							<div class="form-group">
								<label>Enquadramento:</label>
								<p>7455</p>
							</div>
						</div>
						<div class="col-lg-7">
							<div class="form-group">
								<label>Descrição da Infração:</label>
								<p>Transitar em até 20% acima da velocidade permitida</p>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Tipo Infração:</label>
								<p>Média (4 pontos)</p>
							</div>
						</div>
					</div>
					<div class="row">
					<hr>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Ident. Equipamento:</label>
								<p>ABC123sda</p>
							</div>
						</div>
						<div class="col-lg-2">
							<div class="form-group">
								<label>Data da Infração:</label>
								<p>00/00/0000 00:00</p>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Ident. Equipamento:</label>
								<p>ABC123sda</p>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Local da infração:</label>
								<p>Rua dos aloprados, 12, Goiânia/GO</p>
							</div>
						</div>
					</div>
					<!-- ./row -->
					<div class="row">
					<hr>
						<div class="col-lg-2">
							<div class="form-group">
								<label>Veloc. Permitida:</label>
								<p>00 KM/h</p>
							</div>
						</div>
						<div class="col-lg-2">
							<div class="form-group">
								<label>Veloc. Aferida:</label>
								<p>00 KM/h</p>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Veloc. Considerada:</label>
								<p>00 KM/h</p>
							</div>
						</div>
					</div>
					<!-- row -->
					
					<div class="row">
					<hr>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Cod. Identificação Veicular:</label>
								<p>ACEKC32E8F93JD928RV2839</p>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Proprietário:</label>
								<p>Fulano di Tal da Silva</p>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Placa:</label>
								<p>NFP 6969</p>
							</div>
						</div>
					</div>
					<!-- row -->
				</div>
			</div>
		</div>
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-12 -->
</div>
