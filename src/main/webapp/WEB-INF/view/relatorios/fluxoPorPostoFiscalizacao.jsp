<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Fluxo de Veiculos por Posto de Fiscalização</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row" ng-controller="fluxoPorPostoFiscalizacaoCtrl">
	<div class="col-lg-12">
		<div class="panel-group" id="accordion">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">Buscar</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<label>Posto de fiscalização:</label> <select
									class="form-control" name="">
									<option value="">SELECIONE</option>
									<option value="">Posto de fiscalização 1</option>
									<option value="">Posto de fiscalização 2</option>
									<option value="">Posto de fiscalização com nome
										comprido demais</option>
								</select>
							</div>
						</div>
						<div class="col-lg-2">
							<div class="form-group">
								<label>Data Inicial:</label>
								<p>
									<input class="form-control" type="text" name="dtInicial"
										placeholder="00/00/0000" />
								</p>
							</div>
						</div>
						<div class="col-lg-2">
							<div class="form-group">
								<label>Data Final:</label>
								<p>
									<input class="form-control" type="text" name="dtFinal" placeholder="00/00/0000" />
								</p>
							</div>
						</div>
						<div class="col-lg-2">
							<div class="form-group">
								<br>
								<button class="btn btn-primary" ng-click="">Buscar</button>
							</div>
						</div>
						<!-- col-lg-3 -->
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">Detalhes - <b>Posto Recordista de autuações - 00/00/0000 à 00/00/0000</b></h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-3">
							<div class="form-group">
								<label>Ident. Equipamento:</label>
								<p>ABC123sda</p>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Nome do Posto:</label>
								<p>Posto do recorde de multas</p>
							</div>
						</div>
						<div class="col-lg-1">
							<div class="form-group">
								<label>Data de cadastro:</label>
								<p>00/00/0000</p>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Local de Instalação:</label>
								<p>Rua dos aloprados, 12, Goiânia/GO</p>
							</div>
						</div>
					</div>
					<!-- ./row -->
					<div class="row">
					<hr>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Velocidade Média:</label>
								<p>00 KM/h</p>
							</div>
						</div>
						<div class="col-lg-2">
							<div class="form-group">
								<label>QTDE Veiculos:</label>
								<p>00 Veiculos</p>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Auturações Registradas:</label>
								<p>00 Autuações</p>
							</div>
						</div>
					</div>
					<!-- ./row -->
				</div>
			</div>
		</div>
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-12 -->
</div>
