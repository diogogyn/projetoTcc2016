<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Autuações por posto de fiscalização</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row" ng-controller="autuacoesPorPostoFiscalizacaoCtrl">
	<div class="col-lg-12">
		<div class="panel-group" id="accordion">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">Buscar</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<label>Posto de fiscalização:</label> <select
									class="form-control" name="">
									<option value="">TODOS</option>
									<option value="">Posto de fiscalização 1</option>
									<option value="">Posto de fiscalização 2</option>
									<option value="">Posto de fiscalização com nome
										comprido demais</option>
								</select>
							</div>
						</div>
						<div class="col-lg-2">
							<div class="form-group">
								<label>Data Inicial:</label>
								<p>
									<input class="form-control" type="text" name="dtInicial"
										placeholder="00/00/0000" />
								</p>
							</div>
						</div>
						<div class="col-lg-2">
							<div class="form-group">
								<label>Data Final:</label>
								<p>
									<input class="form-control" type="text" name="dtFinal"
										placeholder="00/00/0000" />
								</p>
							</div>
						</div>
						<div class="col-lg-2">
							<div class="form-group">
								<br>
								<button class="btn btn-primary" ng-click="">Buscar</button>
							</div>
						</div>
						<!-- col-lg-3 -->
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">Autuações encontradas {{indexMessage}}</h4>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel-body">
							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover"
									id="dataTables-autuacoes">
									<thead>
										<tr>
											<th>DATA</th>
											<th>COD VEICULAR</th>
											<th>DESC INFRAÇÃO</th>
											<th>LOCAL</th>
										</tr>
									</thead>
									<tbody>
										<tr class="gradeA">
											<td><a href="#/registroAutuacao">00/00/0000 00:00</a></td>
											<td><a href="#/registroAutuacao">ACEKC32E8F93JD928RV2839</a></td>
											<td><a href="#/registroAutuacao">Transitar em até
													20% acima da velocidade permitida</a></td>
											<td><a href="#/registroAutuacao">Rua dos aloprados,
													12, Goiânia/GO</a></td>
										</tr>
									</tbody>
								</table>
								<!-- /.table-responsive -->
							</div>
							<!-- /.dataTable_wrapper -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->
			</div>
		</div>
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<script>
	$(document).ready(function() {
		$('#dataTables-autuacoes').DataTable({
			responsive : true
		});
	});
</script>
