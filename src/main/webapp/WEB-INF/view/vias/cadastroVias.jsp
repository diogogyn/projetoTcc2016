<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Cadastro de Vias</h1>
	</div>
</div>
<!-- /.row -->
<div class="row" ng-controller="cadastroViasCtrl">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Cadastre ou gerencie uma via de
				transito</div>
			<div class="panel-body">
				<form role="form">
					<div id="collapseInclusaoAlteracao"
						class="panel-collapse collapse in">
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Nome da via:</label> <input class="form-control"
											placeholder="AV, Rua, etc" name="nome_via" ng-model="nome_via">
									</div>
									<div class="form-group">
										<label>Tipo de via:</label>
										<select class="form-control" name="tipo_via" ng-model="tipo_via">
											<option value="EXPRESSA">Tran. Rapido</option>
											<option value="ARTIRIAL">Arterial</option>
											<option value="COLETORA">Coletora</option>
											<option value="OUTRA">Outro</option>
										</select>
									</div>
									<div class="form-group">
										<label>Quadra:</label> <input class="form-control"
											placeholder="00" name="quadra" ng-model="quadra">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>CEP:</label> <input class="form-control"
											placeholder="00.000-000" name="cep" ng-model="cep">
									</div>
									<div class="form-group">
										<label>UF:</label>
										<select class="form-control" name="uf" ng-model="uf">
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>
									<div class="form-group">
										<label>Cidade:</label> <input class="form-control"
											placeholder="cidade" name="cidade" ng-model="cidade">
									</div>
									<div class="form-group">
										<label>Bairro/Setor:</label> <input class="form-control"
											placeholder="Res, Conjunto, Condominio" name="bairro" ng-model="bairro">
									</div>
									<div class="form-group">
										<label>Veloc Max permitida:</label> <input
											class="form-control" placeholder="00 KM/h" name="vel_max" ng-model="vel_max">
									</div>
								</div>
							</div>
						</div>
					</div>
					<button ng-click="cadastrarVia()" class="btn btn-primary">Salvar</button>
				</form>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
</div>
