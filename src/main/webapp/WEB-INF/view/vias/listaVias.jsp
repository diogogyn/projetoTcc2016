<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Gerenciamento de Vias de Transito</h1>
	</div>
</div>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"></div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-3">
						<div class="form-group">
							<br> <a href="#/cadastroVias"
								class="form-control btn btn-success"><i class="fa fa-plus"></i>
								Cadastrar nova Via</a>
						</div>
					</div>
					<!-- /.col-lg-3 -->
				</div>
				<!-- row -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel panel-default -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="well">
			<div class="row" ng-controller="listaViasCtrl">
				<div class="col-lg-4"  ng-repeat="via in vias">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<div class="row">
								<div class="col-lg-8">{{via.nomeVia}}</div>
								<div class="col-lg-4">
									<a href="#/cadastroVias" class="btn btn-warning btn-xs">Gerenciar</a>
								</div>
							</div>
							<!-- /.row -->
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-3">
									<img src="http://fakeimg.pl/60/?text=IMG">
								</div>
								<!-- /.col-lg-4 -->
								<div class="col-lg-8">
									<p>
										<b>Bairro:</b> {{via.bairro}}<br> <b>Quadra:</b>
										{{via.quadra}}<bt><b>Possui Fiscalização: </b>SIM
								</div>
								<!-- /.col-lg-8 -->
							</div>
							<!-- /.row -->
						</div>
						<div class="panel-footer">
							<b>Cidade/Estado:</b> {{via.cidade}}/{{via.uf}}
						</div>
					</div>
				</div>
				<!-- /.col-lg-4 -->
				
			</div>
			<!-- /.row -->
		</div>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
