<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Cadastro</h1>
	</div>
</div>
<!-- /.row -->
<div class="row" ng-controller="listarPostosFiscalizacaoCtrl">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"></div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-3">
						<div class="form-group">
							<br> <a href="#/cadastroProfissional"
								class="form-control btn btn-success">
								<i class="fa fa-plus"></i>
								Cadastrar novo Posto</a>
						</div>
					</div>
					<!-- /.col-lg-3 -->
				</div>
				<!-- row -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel panel-default -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="well">
			<div class="row">
				<div class="col-lg-4" ng-repeat="posto in listaPostosFiscalizacao">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<div class="row">
								<div class="col-lg-8">#{{posto.id}} - {{posto.nome}}</div>
								<div class="col-lg-4">
									<a href="gerencia_paciente.html" class="btn btn-warning btn-xs">Gerenciar</a>
								</div>
							</div>
							<!-- /.row -->
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-3">
									<img src="http://fakeimg.pl/60/?text=IMG">
								</div>
								<!-- /.col-lg-4 -->
								<div class="col-lg-8">
									<p>
										<b>Local de instalação:</b> {{posto.endereco.nome}}, QD {{posto.endereco.quadra}}, {{posto.endereco.cidade}}/{{posto.endereco.estado}}
										<br>
										<b>Velocidade Max. do trecho: </b>{{posto.endereco.velocidadeVia}} 
										<br>										
								</div>
								<!-- /.col-lg-8 -->
							</div>
							<!-- /.row -->
						</div>
						<div class="panel-footer">
							<a href="gerencia_paciente.html" class="btn btn-success btn-xs">{{posto.status}}</a>
						</div>
					</div>
				</div>
				<!-- /.col-lg-4 -->

			</div>
			<!-- /.row -->
		</div>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
