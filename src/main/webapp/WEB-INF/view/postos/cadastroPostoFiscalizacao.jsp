<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Cadastro de Posto de Fiscalização</h1>
	</div>
</div>
<!-- /.row -->
<div class="row" ng-controller="cadastroPostoFiscalizacaoCtrl" data-ng-init="init()">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-heading">Cadastre ou gerencie um posto de
				fiscalização</div>
			<div class="panel-body">
				<form role="form" name="postoForm">
					<div id="collapseInclusaoAlteracao"
						class="panel-collapse collapse in">
						<div class="panel-body">
							<div clas="row">
							<div class="col-lg-12">
							<div class="alert alert-success" ng-show="msgSuccess!=null">
                                {{msgSuccess}}
                            </div>
							</div>
							</div>
							<div class="row">
								<div class="col-lg-7">
									<div class="form-group">
										<label><i class="fa fa-map-marker fa-fw"></i> Local instalado:</label>
										<select name="via" ng-model="via" class="form-control" 
										ng-options="via.nome + ', ' +via.quadra + ', '+ via.velocidadeVia + ' KM/h, ' + via.cidade for via in vias">
											<option value="">Selecione ...</option>
										</select>
									</div>
									<div class="form-group">
										<label><i class="fa fa-road fa-fw"></i> Distância entre Antenas (em metros):</label>
										<input type="number" name="distancia_antenas" ng-model="distancia_antenas" class="form-control" placeholder="ex: 25" ng-required="true">
									</div>
								</div>

								<div class="col-lg-5">
									<div class="form-group">
										<label><i class="fa fa-info fa-fw"></i> Nome:</label> 
										<input type="text" class="form-control" name="nome_posto" ng-model="nome_posto" placeholder="um nome que facilite a identificação" ng-required="true">
									</div>

									<div class="form-group">
										<label><i class="fa fa-sitemap fa-fw"></i> End. IP:</label>
										<input type="text" class="form-control" name="endereco_ip" ng-model="endereco_ip" placeholder="000.000.000.000" ng-required="true">
									</div>
									<div class="form-group">
										<button ng-click="cadastrarPostoFiscalizacao()" class="btn btn-primary" ng-disabled="postoForm.$invalid">Salvar</button>
									</div>
								</div>
							</div>
						</div>
					</div>

				</form>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<div class="col-lg-4 col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-bell fa-fw"></i> Instruções
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<ul>
					<li><i class="fa fa-road fa-fw"></i> Porque informar a distancia:
						<ul>
							<li>A distância entre as antenas, é utilizado para calular a velocidade média dos veiculos. A distancia utilizada é em metros (m)</li>
						</ul>
					</li>
					<li><i class="fa fa-info fa-fw"></i> Identificação do posto:
						<ul>
							<li>Informar um nome para o posto, facilita a identificação do mesmo na utilização do sistema. Informe um nome único.</li>
						</ul>
					</li>
					<li><i class="fa fa-map-marker fa-fw"></i> Local de instalação:
						<ul>
							<li> Um posto de fiscalização pode ser atribuido a um endereço cadastrado no sistema. Uma avenida pode possuir n postos de fiscalização desde que em quadras diferentes</li>
						</ul>
					</li>
					<li><i class="fa fa-sitemap fa-fw"></i> Endereço IP:
						<ul>
							<li>Endereço utilizado para conectar a leitora RFID para executar a leitura das etiquetas e processamento dos dados.</li>
						</ul>
					</li>
				</ul>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-4 col-md-4 -->
</div>
