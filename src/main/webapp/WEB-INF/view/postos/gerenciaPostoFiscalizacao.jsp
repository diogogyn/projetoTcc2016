<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Gerenciamento do Posto de Fiscalização</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row" ng-controller="gerenciaPostoFiscalizacaoCtrl">
	<div class="col-lg-8">
		<div class="panel-group" id="accordion">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">{{postoModel.nome}}</h4>
				</div>
				<div class="row">
					<div class="panel-body">
						<div class="col-lg-4">
							<div class="form-group">
								<a ng-click="ligarPostofiscalizacao(postoModel)" class="btn btn-primary btn-md">Ligar Posto de
									Fiscalização</a>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<a ng-click="desligarPostofiscalizacao(postoModel)" class="btn btn-danger btn-md">Desativar Posto</a>
							</div>
						</div>
						<div class="col-lg-2">
							<div class="form-group">
								<a href="#/cadastroPostoFiscalizacao" class="btn btn-primary btn-md">Editar Configs</a>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="alert alert-success" ng-show="msgServidor!=null">
                                {{$msgServidor}}
                            </div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel-body">
							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover"
									id="dataTables-receitasPrescritas">
									<thead>
										<tr>
											<th>Ultimas Mensagens</th>
										</tr>
									</thead>
									<tbody>
										<tr class="gradeA" ng-repeat="mensagem in msgServidor">
											<td>{{mensagem.msg}}</td>
										</tr>
										<tr class="gradeA" ng-show="msgServidor==null">
											<td>Nenhuma mensagem a mostrar</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
							<!-- /.panel-body -->
						</div>
						<!-- /.panel -->
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->
			</div>
		</div>
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-8 -->
	<div class="col-lg-4 col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-bell fa-fw"></i> Resumo
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="list-group">
					<a href="#" class="list-group-item"> <i
						class="fa fa-laptop fa-fw"></i> Tempo ativo <span
						class="pull-right text-muted small"><em>4 minutes ago</em>
					</span>
					</a> <a href="#" class="list-group-item"> <i
						class="fa fa-upload fa-fw"></i> Veilulos capturados <span
						class="pull-right text-muted small"><em>12 veiculos</em> </span>
					</a> <a href="#" class="list-group-item"> <i
						class="fa fa-warning fa-fw"></i> Autuações geradas! <span
						class="pull-right text-muted small"><em>1 autuação</em> </span>
					</a> <a href="#" class="list-group-item"> <i
						class="fa fa-warning fa-fw"></i> Queda de Servidor! <span
						class="pull-right text-muted small"><em>11:13 AM</em> </span>
					</a>
				</div>
				<!-- /.list-group -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-4 col-md-4 -->
	<script>
		$(document).ready(function() {
			$('#dataTables-buscaPacientes').DataTable({
				responsive : true
			});
		});
		$(document).ready(function() {
			$('#dataTables-itensReceita').DataTable({
				responsive : true
			});
		});
		$(document).ready(function() {
			$('#dataTables-receitasPrescritas').DataTable({
				responsive : true
			});
		});
	</script>
</div>
