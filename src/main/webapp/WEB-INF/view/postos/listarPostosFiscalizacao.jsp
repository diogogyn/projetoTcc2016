<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Postos de fiscalização Cadastrados</h1>
	</div>
</div>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Menu</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">					
					<!-- /.col-lg-2 -->
					<div class="col-lg-4">
						<div class="form-group">
							<br> <a href="#/cadastroProfissional"
								class="form-control btn btn-success"><i class="fa fa-plus"></i>
								Novo Posto de Fisãcalização</a>
						</div>
					</div>
					<!-- /.col-lg-3 -->
				</div>
				<!-- row -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel panel-default -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="well">
			<div class="row" ng-controller="listarPostosFiscalizacaoCtrl" data-ng-init="init()">
				<div class="col-lg-4" ng-repeat="posto in postos">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<div class="row">
								<div class="col-lg-8">{{posto.nome}}</div>
								<div class="col-lg-2">
									<a href="#/gerenciaPostoFiscalizacao/{{posto.idPosto}}" class="btn btn-warning btn-xs">Gerenciar</a>
								</div>
							</div>
							<!-- /.row -->
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-3">
									<img src="http://fakeimg.pl/60/?text=IMG">
								</div>
								<!-- /.col-lg-4 -->
								<div class="col-lg-8">
									<p>
										<b>Local de instalação: </b> {{posto.endereco.nome}}, {{posto.endereco.quadra}}, {{posto.endereco.cidade}}
										<br>
										<b>Velocidade da Via: </b>{{posto.endereco.velocidadeVia}}
								</div>
								<!-- /.col-lg-8 -->
							</div>
							<!-- /.row -->
						</div>
						<div class="panel-footer">
							<b>Status:</b> <p class="btn btn-success btn-xs">{{posto.status}}</p>
						</div>
					</div>
				</div>
				<!-- /.col-lg-4 -->

			</div>
			<!-- /.row -->
		</div>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
