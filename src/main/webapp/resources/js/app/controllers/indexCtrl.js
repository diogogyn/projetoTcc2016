app.controller("indexCtrl", function($scope, $http){
	
    console.log("está funcionando perfeitamente a minha estrutura em angular js com spring mvc!!!");
    // VARIABLES =============================================================
    var TOKEN_KEY = "jwtToken"
    var $notLoggedIn = $("#notLoggedIn");
    var $loggedIn = $("#loggedIn").hide();
    var $loggedInBody = $("#loggedInBody");
    var $response = $("#response");
    var $login = $("#login");
    var $userInfo = $("#userInfo").hide();
    var $formCertificate = $("#formCertificate");
    var $certificates = $("#certificates");
    var $certificateDetail = $("#certificateDetail");
    
    var SCHEMA = 'https';
	var API_URI = '/';
	var host;
	var port;
	var api_url;
	var api_key;
	var api_secret;
	var debug = false;
    
    //this.host = 'localhost';
    //this.port = '8080';
    //this.api_url = SCHEMA + '://' + host + ':' + port + API_URI;
    
    function construct() {
    	this.host = 'localhost';
        this.port = 8080;
        this.api_url = SCHEMA + '://' + host + ':' + port + API_URI;
        this.api_key = 'user';
        this.api_secret = 'e0ade237-545b-4dfe-89d1-84edfe79d472';
        this.nonce = '4314efa9-04c2-4109-a6a6-385797fa47a3';
        this.debug = true;
        
        console.log("Passando pelo construtor");
    }
    
    function setDebug(debug){
        this.debug = debug;
    }
    
    function setApiKey(api_key){
        this.api_key = api_key;
        localStorage.setItem(api_key, api_key);
    }
    
    function getApiKey() {
        return localStorage.getItem(api_key);
    }
    
    function setApiSecret(api_secret){
        this.api_secret = api_secret;
        localStorage.setItem(api_secret, api_secret);
    }
    
    function getApiSecret() {
        return localStorage.getItem(api_secret);
    }
        
    // FUNCTIONS =============================================================
    function getJwtToken() {
        return localStorage.getItem(TOKEN_KEY);
    }

    function setJwtToken(token) {
        localStorage.setItem(TOKEN_KEY, token);
    }

    function removeJwtToken() {
        localStorage.removeItem(TOKEN_KEY);
    }

    function doLogin(loginData) {
        $.ajax({
            url: "/auth",
            type: "POST",
            data: JSON.stringify(loginData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                setJwtToken(data.token);
                $login.hide();
                $notLoggedIn.hide();
                showTokenInformation();
                showUserInformation();
                listCertificates();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 401) {
                    $('#loginErrorModal')
                        .modal("show")
                        .find(".modal-body")
                        .empty()
                        .html("<p>Spring exception:<br>" + jqXHR.responseJSON.exception + "</p>");
                } else {
                    throw new Error("an unexpected error occured: " + errorThrown);
                }
            }
        });
    }

    function sendCertificate(certificateData) {
        console.log("tentando enviar os dados para a API");
        $.ajax({
            url: "/insertCertificate",
            type: "POST",
            data: JSON.stringify(certificateData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: createAuthorizationTokenHeader(),
            success: function (data, textStatus, jqXHR) {
                listCertificates();// para atualizar a lista de certificados
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 401) {
                    $('#loginErrorModal')
                        .modal("show")
                        .find(".modal-body")
                        .empty()
                        .html("<p>Spring exception:<br>" + jqXHR.responseJSON.exception + "</p>");
                } else {
                    throw new Error("an unexpected error occured[CERTIFICADO]: " + errorThrown);
                }
            }
        });
    }
    
    function listEcho() {
        console.log("Pasosu em listEcho()");
    	var date = new Date();
    	var data=JSON.stringify(data);
    	$certificates.show();
        $certificateDetail.hide();
        $http({
            url: "/echo",
            type: "POST",
            Accept: "application/json",
            Date: date,
            contentType: "application/json",
            ContentLength:  data.length,
            dataType: "json",
            Authorization: createAuthorizationTokenHeader("/echo", date, data)
        }).success(function (data, textStatus, jqXHR) {
            //console.log("listando os certificados");
            console.log("Recebi do servidor: asf "+data);
        });
    }
    
    function listCertificates() {
        $certificates.show();
        $certificateDetail.hide();
        $http({
            url: "/listCertificates",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: createAuthorizationTokenHeader()
        }).success(function (data, textStatus, jqXHR) {
            //console.log("listando os certificados");
            $scope.certificados = data;
        });
    }

    function doLogout() {
        removeJwtToken();
        $login.show();
        $userInfo
            .hide()
            .find("#userInfoBody").empty();
        $loggedIn.hide();
        $loggedInBody.empty();
        $notLoggedIn.show();
        $formCertificate.hide();
        $certificates.hide();
    }

    function createAuthorizationTokenHeader(uri, date, payload) {
        /*var token = getJwtToken();
        if (token) {
            return {"Authorization": createAuthHeader(uri, date, payload)};
        } else {
            return {};
        }*/
    	return createAuthHeader(uri, date, payload);
    }
    
    function createAuthHeader(uri, date, payload)
    {
        var nonce = uniqid();
        var string_to_sign =
            "POST\n" +
            SCHEMA + "\n" +
            this.host + ':' + this.port + "\n" +
            uri + "\n" +
            "application/json\n" +
            this.api_key + "\n" +
            nonce + "\n" +
            date + "\n" +
            payload + "\n";

        if (this.debug) {
        	console.log("Signing:\n" + string_to_sign);
        }
        console.log("Signing:\n" + string_to_sign);
        //var hmac = cryptoJS.createHmac('sha512', api_secret);
        var hmac = CryptoJS.HmacSHA256(key+timestamp, apiSecretKey);
        
        hmac.write(string_to_sign);
    	hmac.end()
    	//return hmac.read();
    	console.log(hmac.read());
        //var digest = hash_hmac('sha512', string_to_sign, this.api_secret, true);
        
        return 'HmacSHA512 ' + api_key + ':' + nonce + ':' + base64UrlEncode(hmac.read());
    }
    
    var testRequest = function() {
        var data = { test: 'test'}
        var timestamp = getMicrotime(true).toString();
        $.ajax({
            type: "GET",
            url: baseURL + "/test",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function (request) {
                request.setRequestHeader('X-MICROTIME', timestamp);
                request.setRequestHeader('X-HASH', getHMAC(api_secret, timestamp));
            },    
            data: JSON.stringify(data),
            success: function (data) {
                alert(data.message);
            },
            error: function (errorMessage) {
               if(errorMessage.status == 401)
                   alert('Access denied');
            }
        });
    };
 
    var readCookie =  function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    };  
     
    var getHMAC = function(key, timestamp) {
        var hash = CryptoJS.HmacSHA1(api_secret+timestamp, apiSecretKey);
        return hash.toString();
    };
 
    var getMicrotime = function (get_as_float) {
 
      var now = new Date().getTime() / 1000;
      var s = parseInt(now, 10);
 
      return (get_as_float) ? now : (Math.round((now - s) * 1000) / 1000) + ' ' + s;
    };
    
    function showUserInformation() {
        $userInfo.show();
        $formCertificate.show();
    }

    function showTokenInformation() {
        var jwtToken = getJwtToken();
        var decodedToken = jwt_decode(jwtToken);
        $loggedIn.show();
    }

    function appendKeyValue($table, key, value) {
        var $row = $("<tr>")
            .append($("<td>").text(key))
            .append($("<td>").text(value));
        $table.append($row);
    }

    function showResponse(statusCode, message) {
        $response
            .empty()
            .text("status code: " + statusCode + "\n-------------------------\n" + message);
    }

    // REGISTER EVENT LISTENERS =============================================================
    $("#loginForm").submit(function (event) {
        event.preventDefault();

        var $form = $(this);
        var formData = {
            username: $form.find('input[name="username"]').val(),
            password: $form.find('input[name="password"]').val()
        };

        doLogin(formData);
    });

    $("#logoutButton").click(doLogout);

    $("#certificateForm").submit(function (event) {
        event.preventDefault();

        var $form = $(this);
        var formData = {
            name: $form.find('input[name="inputName"]').val(),
            certificateFile: $form.find('input[name="certificateFile"]').val()
        };
        var certificadoModel =  new Object();
        certificadoModel.name = formData.name;
        certificadoModel.certificateFile = formData.certificateFile;//procurar como enviar o full path
        console.log("Recebi os dados no js");
        console.log(certificadoModel);
        sendCertificate(certificadoModel);
    });

    $loggedIn.click(function () {
        $loggedIn
            .toggleClass("text-hidden")
            .toggleClass("text-shown");
    });

    $scope.showCertificateDetail = function(certificate){
        $certificateDetail.show();
        //$scope.certificateDescription = certificate;
        $certificateDetail.show();
        console.log("ID: "+certificate.id);
        $http({
            url: "/viewCertificate/" + certificate.id,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: createAuthorizationTokenHeader()
        }).success(function (data, textStatus, jqXHR) {
            //listCertificates();// para atualizar a lista de certificados
            //console.log(data);
            $scope.certificateDescription = data;
            console.log(data);
        }).error(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 401) {
                $('#loginErrorModal')
                    .modal("show")
                    .find(".modal-body")
                    .empty()
                    .html("<p>Spring exception:<br>" + jqXHR.responseJSON.exception + "</p>");
            } else {
                throw new Error("Um erro ocorreu ao tentar excluir: " + errorThrown);
            }
        });
    }

    $scope.deleteCertificate = function(certificate){
        $certificateDetail.show();
        console.log("ID: "+certificate.id);
        $http({
            url: "/deleteCertificate/" + certificate.id,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: createAuthorizationTokenHeader()
        }).success(function (data, textStatus, jqXHR) {
            listCertificates();// para atualizar a lista de certificados
            console.log(data);
        }).error(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 401) {
                $('#loginErrorModal')
                    .modal("show")
                    .find(".modal-body")
                    .empty()
                    .html("<p>Spring exception:<br>" + jqXHR.responseJSON.exception + "</p>");
            } else {
                throw new Error("Um erro ocorreu ao tentar excluir: " + errorThrown);
            }
        });
    }
    // INITIAL CALLS =============================================================
    construct();
    if (getApiKey() && getApiSecret()) {
        $login.hide();
        $notLoggedIn.hide();
        showTokenInformation();
        showUserInformation();
        listEcho();
    }
    
})
